# Annulus Flask Project
## A web server to create the G-code to cut an annulus

Uses Flask and Python

Heroku link:  
`https://g-code-annulus.herokuapp.com/ | https://git.heroku.com/g-code-annulus.git`

Simon Hewitt
07799 381001
simon.d.hewitt@gmail.com
October 2018

Displays a single form which asks for the measurements for the annulus, 
then generates the G-code to cut it, into a display panel    
Run with `flask run` in the code directory    
`/Users/simonhewitt/Google Drive/ModelEngineering/Machines/CNC Warco Mill/MySoftware/annulus-flask`  

Cut and paste to use it  


ToDo:

1. Tool Tips
1. Download a file (have to use cut and paste into a native editor at the moment)
1. Remove trailing .0 in 22.0 i.e. 22.0 -> 22, 22.01 stet **Done**
1. Fix decimal places - 3 places? No trailing zeroes. **Done**
1. stats! **Mostly Done** - add duration please!
1. Correct direction for inner cuts, g3 fom g2 **Done**

Simon Hewitt  
07799 381001  
simon.d.hewitt@gmail.com  

