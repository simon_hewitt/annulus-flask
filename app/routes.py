import flask

from app import app
from flask import render_template, flash, redirect, url_for, send_file
from app.forms import AnnulusCalcForm
from annulus import calc_annulus
import io

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    g_code_dict = {
        'g_code': ['G-code will go here','on several lines'],
        'stats': ['Stats will go here']
    }

    form = AnnulusCalcForm()
    if form.validate_on_submit():
        g_code_dict = calc_annulus(form)
        g_code_str = '\n'.join(g_code_dict['g_code'])
        # Prep to download file
        fp = io.StringIO(g_code_str)
        # return send_file(fp, attachment_filename='annulus.tap')

    return render_template('annulus.html', data=g_code_dict, form=form)

@app.route('/help', methods=['GET'])
def help():
    return render_template('readme.html')
