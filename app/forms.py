from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, \
    IntegerField, validators, DecimalField, TextAreaField, TextField, RadioField, SelectField
from wtforms.validators import DataRequired


class AnnulusCalcForm(FlaskForm):
    cutter_dia = DecimalField('Cutter Diameter', [validators.required()], places=2)
    wcs = SelectField('Select the WCS', choices=[('G54', 'G54'), ('G55', 'G55'), ('G56', 'G56'), ('G57', 'G57'),
                                                 ('G58', 'G58'), ('G59', 'G59')])
    annulus_inner_dia = DecimalField('Annulus inner diameter', [validators.required()], places=2)
    annulus_outer_dia = DecimalField('Annulus outer diameter', [validators.required()], places=2)
    annulus_depth = DecimalField('Overall cut depth', [validators.required()], places=2)
    cut_depth = DecimalField('Per cut depth', [validators.required()], places=2, default=1)
    cut_side_feed = DecimalField('Radius increase per cut', [validators.required()], places=2, default=0.5)
    feed_rate = DecimalField('Feed (mm/min)', [validators.required()], places=2, default=250)
    cut_direction = RadioField('Climb or Not',
                               choices=[('climb', 'Climb'), ('conventional', 'Conventional')],
                               default='conventional')
    # stats = TextAreaField('Cutting Statistics')
    submit = SubmitField('Calculate')
