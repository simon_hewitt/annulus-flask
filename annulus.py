'''
Python 3.7 and Flask
Creates G code to machine an annulus
Simon Hewitt
07799 381001
simon.d.hewitt@gmail.com
October 2018

Displays a single form which asks for the measurements for the annulus,
then generates the G-code to cut it, into a display panel
Cut and paste to use it

'''

from dataclasses import dataclass
import math
import datetime

preamble = \
    ''' (Created by annulus.py program Simon Hewitt October 2018)
    G90    (Absolute coordinates)
    G94    (Feed in units/minute)
    G91.1  (Arc measuremenmts in incrementa mode)
    G40    (Turn cutter compensation OFF)
    G49    (Turn Toll length offset OFF)
    G17    (XY plane)
    M5     (Spindle OFF)
    M9     (Coolant OFF)
    '''

'''
Other notes:
Does not use tool length - assumes start_z is the absolute top of the job. 
'''

ROUND_DIGITS = 5
Z_CLEAR = 20
Z_FINISH_CLEAR = 100
Z_CUT_READY = 1
STEPOVER = 0.4 # Fraction of the cutter width move before next cut downwards

@dataclass
class GCoder:
    x: float = 0.0
    y: float = 0.0
    z: float = 0.0
    x_centre: float = 0.0
    y_centre: float = 0.0
    radius: float = 0.0
    cutter_diameter: float = 0.0
    arc_centre_set = True
    gcode_list = ''
    gcode_stats = {'max_depth': 0,
                   'moves': 0,
                   'turns': 0}
    error_message: str = ''
    error_status: int = 0
    feed_rate: float = 250
    wcs: str = 'G54'

    def __post_init__(self):
        # Write the preamble out
        for p in preamble.split('\n'):
            self.write_gc(p)
        self.write_comment(datetime.datetime.now().strftime("%a %d %b %Y %H:%M"))
        self.write_gc(self.wcs)

    def _reset_errors(self):
        self.error_message = ''
        self.error_status = 0

    def _set_error(self, mesg, status):
        self.error_message = mesg
        self.error_status = status
        self.gcode_stats['errors'] = mesg
        self.gcode_stats['status'] = status

    def _update_stats(self, moves: int = 1, turns: float = 0, depth: float = 0.0):
        if depth < self.gcode_stats['max_depth']:
            self.gcode_stats['max_depth'] = depth
        self.gcode_stats['moves'] += moves
        self.gcode_stats['turns'] += turns

    def write_gc(self, gc_str: str):
        # gc_str = re.sub(r'\.0([^\d]|$)', ' ', gc_str)
        self.gcode_list += gc_str + '\n'
        print(gc_str)

    def write_feed_rate(self, feed_rate):
        self.feed_rate = feed_rate
        self.write_comment('Feed Rate: {} [mm/min]'.format(self.feed_rate))
        self.write_gc('F {}'.format(self.rnd_to_str(self.feed_rate)))

    def write_comment(self, comment):
        comment.replace('(', '[')
        comment.replace(')', ']')
        self.write_gc('( ' + comment + ' )')

    def rnd(self, x):
        new_x = round(x, ROUND_DIGITS)
        if new_x == -0.0:
            new_x = 0.0
        return new_x

    def rnd_to_str(self, x):
        s = str(self.rnd(x))
        if s[-2:] == '.0':
            s = s[:-2]
        return s

    def move(self, rapid: bool, x=None, y=None, z=None):
        # Rapid from current xo-ords to new, and save new
        gc_str = 'G0 ' if rapid else 'G1 '
        if x is not None:
            gc_str += ' X{}'.format(self.rnd_to_str(x))
            self.x = x
        if y is not None:
            gc_str += ' Y{}'.format(self.rnd_to_str(y))
            self.y = y
        if z is not None:
            gc_str += ' Z{}'.format(self.rnd_to_str(z))
            self.z = z
        self._update_stats()
        self.write_gc(gc_str)

    def xy_distance(self, delta_x: float, delta_y: float):
        x = ((delta_x - self.x) ** 2) + ((delta_y - self.y) ** 2)
        return self.rnd(math.sqrt(x))

    def set_radius(self):
        self.radius = self.xy_distance(self.x_centre, self.y_centre)

    def to_rect(self, r: float, theta: float):
        """
        Polar to cartesian, assuming origin 0,0
        theta in degrees

        returns tuple; (float, float); (x,y)
        """
        x = self.rnd(r * math.cos(math.radians(theta)))
        y = self.rnd(r * math.sin(math.radians(theta)))
        return x, y

    def to_polar(self, x: float, y: float):
        """
        Cartesian to polar, asumes origin 0,0
        returns r, theta(degrees)
        """
        r = (x ** 2 + y ** 2) ** .5
        if y == 0:
            theta = 180 if x < 0 else 0
        elif x == 0:
            theta = 90 if y > 0 else 270
        else:
            theta = math.degrees(math.atan(float(y) / x))
        return self.rnd(r), self.rnd(theta)

    def to_self_polar(self):
        # Current X,Y co-ords as polar (assume origin 0,0)
        return self.to_polar(self.x, self.y)

    def start_arc_abs(self, x_centre: float, y_centre: float):
        # Start an arc, just sets the centre
        self.arc_centre_set = True
        self.x_centre = x_centre
        self.y_centre = y_centre
        self.set_radius()

    def start_arc_delta(self, x_delta: float, y_delta: float):
        # Start an arc, with relative values from current position to arc cntre
        self.x_centre += x_delta
        self.y_centre += y_delta
        self.set_radius()

    def g2_arc(self, degrees: float, z_delta: float):
        '''
        Writes an arc from current XY to new XY calculated by degrees, and adds Z delta.
        Updates new X, Y
        :param degrees: 0.0..360.0, but recommend <= 90.0. g2 (CW) if degress > 0, G3 (ACW) if < 0
        :param z_delta: Cut - -ve means cut!
        '''
        assert self.arc_centre_set, 'g2_arc called before arc set up with start_arc_XXX '
        r, theta = self.to_self_polar()
        theta = (theta - degrees) % 360
        x, y = self.to_rect(r, theta)
        self.z += z_delta
        g_command = 'G2' if degrees >= 0 else 'G3'
        self.write_gc('{} X{} Y{} R{} Z{}'.format(g_command,
                                                  self.rnd_to_str(x),
                                                  self.rnd_to_str(y),
                                                  self.rnd_to_str(self.radius),
                                                  self.rnd_to_str(self.z)))
        self.x = x
        self.y = y
        self._update_stats(turns=0.25, depth=self.z)

    def g2_circle(self, delta_z: float, direction:str = 'CW'):
        for _ in range(4):
            quadrant = 90 if direction == 'CW' else -90
            self.g2_arc(quadrant, delta_z)

    def _goto_circle_start(self, radius):
        self.move(z=Z_CLEAR, rapid=True)
        self.move(x=radius, y=0, rapid=True)
        self.move(z=Z_CUT_READY, rapid=True)

    def g2_circles(self, circle_count: int, radius: float, final_z: float, retract: bool = True, direction: str = 'CW'):
        z_per_quadrant = final_z / (circle_count * 4)
        # Got0 start, y=0 x=radius
        self._goto_circle_start(radius)
        self.radius = radius
        quadrant = 90 if direction == 'CW' else -90

        while self.z > final_z:  # For each quadrant
            self.g2_arc(quadrant, z_per_quadrant)
        # and a final circle at full depth
        self.g2_circle(delta_z=0, direction=direction)
        if retract:
            self.move(z=Z_CLEAR, rapid=True)  # and RETRACT

    def gcode_stats_list(self):
        stats_list = []
        for key, value in self.gcode_stats.items():
            stats_list.append('{} : {}'.format(key, value))

        return stats_list

    def cut_annulus(self, inner_d, outer_d, depth, max_cut, side_feed):
        self._reset_errors()
        self.write_comment('Cutting an Annulus inner diamater: {}, outer diameter: {}, depth: {}'.format(inner_d, outer_d, depth))
        self.write_comment('Max cut is {}'.format(max_cut))

        width = (outer_d - inner_d) / 2.0
        if width * 0.8 < self.cutter_diameter:
            self._set_error('Annulus width too narrow for cutter, width:{}, cutter dia: {}'.format(width, self.cutter_diameter), 1)
            self.gcode_list = 'ERROR'
            return False

        cut_1_r = (outer_d + inner_d) / 4.0  # Central, 1st cut
        cuts = math.ceil(depth / max_cut)
        outer_radius = (outer_d / 2.0) - (self.cutter_diameter / 2.0)
        inner_radius = (inner_d / 2.0) + (self.cutter_diameter / 2.0)
        self.start_arc_abs(0, 0)

        # First cut, on centre line of the annulus, at increments to full depth.
        self.write_comment('Cut first groove to depth')
        self.g2_circles(cuts, cut_1_r, -depth, retract=False)

        self.write_comment("move OUT by small increments until outer limit is cut")
        radius = min(outer_radius, cut_1_r + (self.cutter_diameter * STEPOVER))

        while radius <= outer_radius:
            self._goto_circle_start(radius)
            self.g2_circles(cuts, radius, -depth, retract=False)
            radius += self.cutter_diameter * STEPOVER
        # and a final outer cut
        self.g2_circles(cuts, outer_radius, -depth, retract=False)

        self.write_comment("move IN by small increments until inner limit is cut")
        radius = max(inner_radius, cut_1_r - (self.cutter_diameter * STEPOVER))
        while radius >= inner_radius:
            self._goto_circle_start(radius)
            self.g2_circles(cuts, radius, -depth, retract=False, direction='ACW')
            radius -= self.cutter_diameter * STEPOVER
        # and a final inner cut
        self.g2_circles(cuts, inner_radius, -depth, retract=False, direction='ACW')

        self.move(z=Z_FINISH_CLEAR, rapid=True)
        self.move(x=0, y=0, rapid=True)
        self.write_comment('ALL DONE')

        return True


def calc_annulus(form):
    annulus_inner_diameter = float(form.annulus_inner_dia.data)
    annulus_outer_diameter = float(form.annulus_outer_dia.data)
    annulus_depth = float(form.annulus_depth.data)
    cutter_dia = float(form.cutter_dia.data)
    cut_depth = float(form.cut_depth.data)
    cut_direction = form.cut_direction.data
    side_feed = float(form.cut_side_feed.data)
    feed_rate = float(form.feed_rate.data)
    wcs = form.wcs.data

    gcoder = GCoder(cutter_diameter=cutter_dia, wcs=wcs)
    gcoder.write_feed_rate(feed_rate)

    ok = gcoder.cut_annulus(inner_d=annulus_inner_diameter,
                            outer_d=annulus_outer_diameter,
                            depth=annulus_depth,
                            max_cut=cut_depth,
                            side_feed=side_feed)

    annulus_data = {
        'g_code': gcoder.gcode_list.split('\n'),
        'stats': gcoder.gcode_stats_list(),
        'error_status': gcoder.error_status,
        'error_message': gcoder.error_message
    }
    return (annulus_data)
